﻿# cd C:\Users\Tore\Dropbox\SourceTreeRepros\pskurs
$data = @()
$data += New-Object -TypeName psobject -Property @{Name       ="Get-Test"
                                                   Category   ="Demo"
                                                   FullName   ="Get-Test.ps1"
                                                   Description="This is a test"
                                                   Download   ="https://bitbucket.org/torgro/pskurs/raw/master/Get-Test.ps1"
                                                  }
$data += New-Object -TypeName psobject -Property @{Name       ="Get-Test2"
                                                   Category   ="Demo"
                                                   FullName   ="Get-Test2.ps1"
                                                   Description="This is a test2"
                                                   Download   ="https://bitbucket.org/torgro/pskurs/raw/master/Get-Test2.ps1"
                                                  }
$data += New-Object -TypeName psobject -Property @{Name       ="Set-Test"
                                                   Category   ="demo2"
                                                   FullName   ="Set-Test.ps1"
                                                   Description="This is set test"
                                                   Download   ="https://bitbucket.org/torgro/pskurs/raw/master/Set-Test.ps1"
                                                  }

function Get-FPfunction
{
[ArgumentCompleter()]
Param(
    [string] $Name = "*"
)

    $data | where {$_.Name -like "$Name"}
}

function Global:Install-FPfunction
{
Param(
    [string] $Download
)   
    $txt =  (New-Object net.webclient).DownloadString($Download)
    $sb = [scriptblock]::Create($txt)
    New-Module -ScriptBlock $sb
}

#Invoke-Expression -Command (New-Object net.webclient).DownloadString("https://bitbucket.org/torgro/pskurs/raw/master/data.ps1")